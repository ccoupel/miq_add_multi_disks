#
# Description: <Method description here>
#

disk_id=$evm.root["ae_state_retries"]+1

prov=$evm.root['miq_provision']
vm=prov.destination
storage=(vm.storages.first.name rescue nil) || (prov.options[:placement_ds_name].last rescue nil)

nb_disks=prov.get_option(:nb_disks)
size=prov.get_option("disk_size_#{disk_id}".to_sym).to_i 

$evm.log(:info,"CC- [#{vm.name}] adding new disk #{disk_id}/#{nb_disks} of #{size} GB in #{storage} => #{disk_id <= nb_disks}") 

vm.add_disk("[#{storage}]", size*1024, :sync => true) if disk_id <= nb_disks

$evm.root["ae_result"]="retry" unless disk_id >= nb_disks
#$evm.root["ae_retry_interval"]=20.seconds
